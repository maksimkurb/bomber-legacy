/* eslint-disable */
/**
 * @author Курбатов Максим
 *
 * === README для тех, кто будет исходники эти смотреть ===
 *
 * !!!! ВНИМАНИЕ !!!!
 * Сразу оговорюсь, что чтобы проект заработал на вашем компьютере (если вы хотите поднять сервер),
 * у вас должен быть HTTPS сертификат!!!!! Это прям обязательное требование, потому что современные браузеры
 * не дадут использовать WebSockets для незащищённых соединений. Но через http://localhost:3000/ должно заработать (в целях отладки разрешили исп. http, а не https).
 *
 * Этот проект использует платформу Meteor в качестве основы.
 * Meteor - по сути фреймворк, который имеет Node.JS под капотом.
 *
 * СИСТЕМНЫЕ ТРЕБОВАНИЯ ДЛЯ УСТАНОВКИ (на Windows):
 * Windows 7+ / Windows Server 2003+
 * PowerShell v2+
 * .NET Framework 4+ (при установки Chocolatey, эта программа попробует установить его сама, если ещё не был установлен)
 *
 * Для установки Meteor в Windows 7+ / Windows Server 2003+ сначала необходимо установить Chocolatey:
 * Для этого введите следующую команду в консоли (cmd.exe) от имени Администратора:
      @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
 * ЛИБО следуйте инструкциям на сайте https://chocolatey.org/install (там написано то же самое, но на английском)
 *
 * Теперь Вам необходимо установить сам Meteor:
 * Для этого введите следующую команду в консоли (cmd.exe) от имени Администратора:
      choco install meteor
 * ЛИБО следуйте инструкциям на сайте https://www.meteor.com/install (да, там тоже эта команда)
 *
 * Ну, вроде на установке и всё.
 *
 * Чтобы запустить сайт, перейдите в папку с ним (в консоли, разумеется) и введите команду:
      meteor run
 *
 * Она скачает зависимости и всё сделает сама, потом сайт будет открываться по адресу http://localhost:3000/
 *
 */

/**
  * Да, в этом index.js я просто написал инструкцию по запуску, тут кода нет. Код находится в подпапках.
  * Кстати, сайт написан на новых стандартах ECMAScript 7+, которые ещё не полностью стандартизированы, и не все браузеры завезли их поддержку,
  * но обратная совместимость осуществляется с помощью транспиляции кода в ES5 (aka JavaScript) с помощью Babel Transpiler
  * Вот приблизительное дерево файлов проекта:
        imports/
          startup/
            client/                    # Загружается только на стороне клиента
            server/                    # Загружается только на стороне сервера
          ui/
            components/                # Компоненты React
            pages/                     # Страницы
        client/
          index.js                     # Основной файл клиентской стороны. (будет загружен в браузере клиента)
          index.html <---------------- # INDEX.HTML тут, но его содержание не слишком-то интересно
        server/
          index.js                     # Основной файл серверной стороны. (будет загружен на сервере)
  *
  */
