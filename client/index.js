// Client entry point, imports all client code

import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import '../imports/startup/client';
import '../imports/startup/both';

import App from '../imports/ui/components/App';

Meteor.startup(() => {
  render(<App />, document.getElementById('render-target'));
});
