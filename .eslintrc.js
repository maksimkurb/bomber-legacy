module.exports = {
  "parser": "babel-eslint",
  "extends": "airbnb",
  "env": {
    "browser": true,
    "node": true,
  },
  "settings": {
    "import/core-modules": [ "meteor" ],
    "import/resolver": "meteor",
  },
  "rules": {
    "react/jsx-filename-extension": 0,
    "no-plusplus": 0,
    "no-continue": 0,
    "no-underscore-dangle": ["error", {
      "allow": ["_id"]
    }]
  }
};
