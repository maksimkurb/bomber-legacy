const theme = {
  colors: {
    main: '#FEFF89',
    alt: '#FF9F68',
    primary: '#F85959',
    secondary: '#AC005D',

    textColor: '#463535',
    bg: '#faffdf',

    loader: '#F85959',
  },
};

export default theme;
