import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { lighten } from 'polished';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router';

import Loader from '../../components/Loader';
import Logo from '../../components/Logo';
import Input from '../../components/Input';
import Button from '../../components/Button';


const Container = styled.div`
  color: ${({ theme }) => theme.colors.textColor};
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const BigLogo = styled(Logo)`
  font-size: 7rem;
  text-align: center;
`;
const LoaderContainer = styled.div`
  flex: 0 0 200px;
  display: flex;
  margin: 0 auto;
  align-items: center;
`;
const Invite = styled.div`
  flex: 0 0 200px;
  margin: 0 auto;
  padding: 1.5rem 3rem;
  border-radius: .25rem;
  background: ${({ theme }) => lighten(0.2, theme.colors.alt)};
  & > * {
    flex: 0;
  }
  & > h4 {
    text-align: center;
    margin-top: 0;
    margin-bottom: 2rem;
  }
  & > .link {
    display: flex;
    align-items: center;
    margin-bottom: 1rem;
    & > span {
      margin-right: 1rem;
    }
    & > input {
      display: inline-block;
      flex: 1;
    }
  }
`;

class HomePage extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
  };

  constructor(...args) {
    super(...args);

    this.state = {
      loading: true,
      slug: '',
    };
  }

  componentDidMount() {
    Meteor.call('room.generateSlug', ((err, slug) => {
      if (err) {
        console.error(err);
        alert('Произошла ошибка, для подробностей смотрите консоль');
      }
      this.setState({ slug, loading: false });
    }));
  }

  handleChange = (e) => {
    const slug = e.target.value.toLowerCase()
      .replace(new RegExp('\\s', 'g'), '-')
      .replace(new RegExp('[^a-zа-я0-9-.ё]', 'g'), '');
    this.setState({ slug });
  }

  handleGo = (e) => {
    e.preventDefault();
    this.props.history.push(`/${encodeURIComponent(this.state.slug)}`);
  }

  render() {
    const disabled = this.state.slug.length === 0;
    return (
      <Container>
        <BigLogo />
        {this.state.loading ? <LoaderContainer><Loader /></LoaderContainer> : (
          <Invite>
            <h4>
                Создайте комнату, начните играть
            </h4>
            <div className="link">
              <span>{Meteor.absoluteUrl()}</span>
              <Input value={this.state.slug} onChange={this.handleChange} maxLength={60} placeholder="Название комнаты (обязательно)" />
            </div>
            <Button onClick={this.handleGo} disabled={disabled}>Создать новую комнату!</Button>
          </Invite>
        )}
      </Container>
    );
  }
}

export default withRouter(HomePage);
