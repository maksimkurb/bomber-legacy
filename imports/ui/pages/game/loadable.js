import Loadable from 'react-loadable';
import { FullScreenLoader } from '../../components/Loader';

export default Loadable({
  loader: () => import('../game'),
  loading: FullScreenLoader,
});
