import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { lighten } from 'polished';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import Room from '../../../api/game/room';
import { MAX_PLAYERS } from '../../../api/game/constants';
import Invitation from '../../components/Invitation';
import GameResults from '../../components/GameResults';
import Game from '../../components/Game';
import Loader from '../../components/Loader';
import Logo from '../../components/Logo';


const Container = styled.div`
  color: ${({ theme }) => theme.colors.textColor};
  height: 100%;
  display: flex;
  flex-direction: column;
`;
const Navbar = styled.div`
  padding: .3rem 2rem;
  flex: 0;
  color: #888;
  background: ${({ theme }) => lighten(0.1, theme.colors.main)};
  box-shadow: 1px 0 2px rgba(0,0,0,0.3);
  z-index: 100;
  & > * {
    margin-right: 0.5rem;
    vertical-align: middle;
  }
  & > *:last-child {
    margin-right: 0;
  }
  & strong {
    font-weight: normal;
    color: #222;
  }
`;
const StyledLogo = styled(Logo)`
  margin-right: 1.5rem;
  display: inline-block;
  font-size: 2rem;
`;
const Content = styled.div`
  position: relative;
  flex: 1 0;
  display: flex;
  margin: auto;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  padding: 16px;
`;

class GamePage extends React.Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        slug: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    loading: PropTypes.bool.isRequired,
    roomExists: PropTypes.bool.isRequired,
    roomActive: PropTypes.bool.isRequired,
    roomEnd: PropTypes.bool.isRequired,
    room: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      players: PropTypes.array.isRequired,
    }),
  };

  static defaultProps = {
    room: null,
  }

  renderContent() {
    if (this.props.loading || !this.props.roomExists) return <Loader />;
    return [
      !this.props.roomActive ? <Invitation key="invitation" room={this.props.room} /> : null,
      this.props.roomEnd ? <GameResults key="results" room={this.props.room} /> : null,
      <Game key="game" room={this.props.room} />,
    ];
  }

  render() {
    const { room, roomExists, match: { params } } = this.props;
    return (
      <Container>
        <Navbar>
          <StyledLogo />
          <span>Комната: <strong>{params.slug}</strong></span>
          <span>Игроки: <strong>{roomExists ? room.players.length : '#'}/{MAX_PLAYERS}</strong></span>
        </Navbar>
        <Content>
          {this.renderContent()}
        </Content>
      </Container>
    );
  }
}

export default withTracker((props) => {
  const roomHandle = Meteor.subscribe('room', props.match.params.slug);
  const loading = !roomHandle.ready();
  const room = Room.findOne({ slug: props.match.params.slug });
  const roomExists = !loading && !!room;
  const roomActive = roomExists && room.active;
  const roomEnd = roomExists && room.players.length > 1 &&
    (room.players.filter(pl => pl.ready && pl.dead).length + 1) >= room.players.length;
  if (!loading && !room) {
    Meteor.call('room.createBySlug', props.match.params.slug);
  }
  return {
    loading,
    room,
    roomExists,
    roomEnd,
    roomActive,
  };
})(GamePage);
