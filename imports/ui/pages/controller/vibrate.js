const vibrate = window.navigator.vibrate ?
  ((...args) => { window.navigator.vibrate(...args); }) : (() => {});

export default vibrate;
