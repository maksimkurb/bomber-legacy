import styled from 'styled-components';

export const Root = styled.div`
  color: ${({ theme }) => theme.colors.textColor};
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content: center;
  align-items: center;
  & > div {
    text-align: center;
  }
`;
export const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  padding: 1em;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  user-select: none;
`;

export const Overlay = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 50;
  background: ${({ theme }) => theme.colors.bg};
  text-align: center;
  justify-content: center;
  align-items: center;
  
  @media screen and (orientation: landscape) {
    &.portraitOnly {
      display: none;
    }
  }

  @media screen and (orientation: portrait) {
    &.landscapeOnly {
      display: none;
    }
  }
`;

const buttonSize = 80;
export const ButtonsContainer = styled.div`
  position: relative;
  width: ${buttonSize * 3}px;
  height: ${buttonSize * 3}px;

  & > button {
    position: absolute;
  }

  & > button[data-act="up"] {
    top: 0;
    left: ${buttonSize}px;
  }
  & > button[data-act="right"] {
    top: ${buttonSize}px;
    left: ${buttonSize * 2}px;
  }
  & > button[data-act="down"] {
    top: ${buttonSize * 2}px;
    left: ${buttonSize}px;
  }
  & > button[data-act="left"] {
    top: ${buttonSize}px;
    left: 0;
  }

  & > button[data-act="plant"] {
    position: relative;
    width: ${buttonSize * 2}px;
    height: ${buttonSize * 2}px;
    margin-top: ${buttonSize / 2}px;
  }
`;

export const Button = styled.button`
  background: ${({ theme }) => theme.colors.alt};
  border: 2px solid ${({ theme }) => theme.colors.primary};
  border-radius: 2px;
  outline: none;
  color: ${({ theme }) => theme.colors.secondary};

  &.square {
    width: ${buttonSize}px;
    height: ${buttonSize}px;
  }

  &.fat {
    font-size: 2rem;
    font-weight: bold;
    padding: 1.4rem;
  }

  & svg {
    fill: ${({ theme }) => theme.colors.secondary};
  }

  &.clicked {
    background: ${({ theme }) => theme.colors.secondary};
    color: ${({ theme }) => theme.colors.alt};
    & svg {
      fill: ${({ theme }) => theme.colors.alt};
    } 
  }
`;
