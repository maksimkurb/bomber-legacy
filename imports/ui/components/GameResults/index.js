import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';

import Button from '../Button';
import { playerImages } from '../../../game/textures';

const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 50;

  padding: 2em;

  background: rgba(250, 251, 209, 0.85);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Header = styled.h1`
  text-align: center;
  color: ${({ theme }) => theme.colors.primary};
`;

const Content = styled.div`
  margin: 1em;
  padding: 1.2em;
  background: rgba(254, 255, 230, 0.76);
  border-radius: 5px;
  color: #460f0f;
  text-align: center;
`;

class GameResults extends React.Component {
  static propTypes = {
    room: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      slug: PropTypes.string.isRequired,
      players: PropTypes.array.isRequired,
    }).isRequired,
  }

  onRestart = () => {
    Meteor.call('room.restart', this.props.room._id);
  }

  render() {
    const { room } = this.props;
    const winnerIdx = room.players.findIndex(pl => !pl.dead);
    return (
      <Container>
        <Header>Игра окончена</Header>
        <Content>
          {
            (winnerIdx === -1) ?
              <p>
                  Ничья!
              </p>
            :
              <div>
                <p>
                  Выиграл игрок #{winnerIdx + 1}
                </p>
                <p>
                  <img src={playerImages[winnerIdx]} alt="Победитель" />
                </p>
              </div>
          }

          <p>
            <Button onClick={this.onRestart}>Повторить игру</Button>
          </p>
        </Content>
      </Container>
    );
  }
}

export default GameResults;
