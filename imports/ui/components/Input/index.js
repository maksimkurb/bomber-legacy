import React from 'react';
import styled from 'styled-components';
import { transparentize } from 'polished';

const StyledInput = styled.input`
  display: block;
  width: 100%;
  padding: .35rem .75rem;
  line-height: 1.5;
  border-radius: .25rem;
  border: 2px solid ${({ theme }) => theme.colors.primary};
  background: #fefefe;
  color: ${({ theme }) => theme.colors.textColor};
  transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
  &:focus {
    outline: 0;
    box-shadow: 0 0 0 0.2rem ${({ theme }) => transparentize(0.5, theme.colors.primary)}; 
  }
`;

function Input(props) {
  return (
    <StyledInput {...props} />
  );
}

export default Input;
