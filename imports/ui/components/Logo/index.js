import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  color: ${({ theme }) => theme.colors.primary};
  text-transform: uppercase;
  font-family: 'Nunito', sans-serif;
  text-shadow:
    rgba(0, 0, 0, 0.1) 0.04em 0.04em 0;
`;

function Logo(props) {
  return (
    <Container {...props}>Bomber</Container>
  );
}

export default Logo;
