import React from 'react';
import styled, { keyframes } from 'styled-components';

const rotatePacmanHalfUp = keyframes`
  0%   { transform: rotate(270deg); }
  50%  { transform: rotate(360deg); }
  100% { transform: rotate(270deg); }
`;

const rotatePacmanHalfDown = keyframes`
  0%   { transform: rotate(90deg); }
  50%  { transform: rotate(0deg); }
  100% { transform: rotate(90deg); }
`;
const pacmanBalls = keyframes`
  75%  { opacity: 0.7; }
  100% { transform: translate(-100px, -6.25px); }
`;

const PacmanLoader = styled.div`
  position: relative;
  & > div:nth-child(2) { animation: ${pacmanBalls} 1s -0.99s infinite linear; }
  & > div:nth-child(3) { animation: ${pacmanBalls} 1s -0.66s infinite linear; }
  & > div:nth-child(4) { animation: ${pacmanBalls} 1s -0.33s infinite linear; }
  & > div:nth-child(5) { animation: ${pacmanBalls} 1s 0s infinite linear; }
  & > div:first-of-type {
    width: 0px;
    height: 0px;
    border-right: 25px solid transparent;
    border-top: 25px solid ${({ theme }) => theme.colors.loader};
    border-left: 25px solid ${({ theme }) => theme.colors.loader};
    border-bottom: 25px solid ${({ theme }) => theme.colors.loader};
    border-radius: 25px;
    animation: ${rotatePacmanHalfUp} 0.5s 0s infinite;
    position: relative;
    left: -30px; }
  & > div:nth-child(2) {
    width: 0px;
    height: 0px;
    border-right: 25px solid transparent;
    border-top: 25px solid ${({ theme }) => theme.colors.loader};
    border-left: 25px solid ${({ theme }) => theme.colors.loader};
    border-bottom: 25px solid ${({ theme }) => theme.colors.loader};
    border-radius: 25px;
    animation: ${rotatePacmanHalfDown} 0.5s 0s infinite;
    margin-top: -50px;
    position: relative;
    left: -30px; }
  & > div:nth-child(3),
  & > div:nth-child(4),
  & > div:nth-child(5),
  & > div:nth-child(6) {
    background-color: ${({ theme }) => theme.colors.loader};
    width: 15px;
    height: 15px;
    border-radius: 100%;
    margin: 2px;
    width: 10px;
    height: 10px;
    position: absolute;
    transform: translate(0, -6.25px);
    top: 25px;
    left: 70px; }
`;

export default function Loader(props) {
  return (
    <PacmanLoader {...props}>
      <div />
      <div />
      <div />
      <div />
      <div />
    </PacmanLoader>
  );
}

const FullScreenLoaderContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
`;
export function FullScreenLoader(props) {
  return <FullScreenLoaderContainer><Loader {...props} /></FullScreenLoaderContainer>;
}
