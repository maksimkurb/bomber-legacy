import React from 'react';
import styled, { css } from 'styled-components';
import { transparentize, readableColor, desaturate, lighten } from 'polished';

const StyledButton = styled.button`
  display: block;
  width: 100%;
  padding: .35rem .75rem;
  line-height: 1.5;
  border-radius: .25rem;
  transition: 
    border-color .15s ease-in-out,
    box-shadow .15s ease-in-out;
  cursor: pointer;


  border: 2px solid ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.colors.primary};

  &:disabled {
    background: ${({ theme }) => desaturate(0.5, theme.colors.primary)};
    border-color: ${({ theme }) => desaturate(0.7, theme.colors.primary)};
    color: ${({ theme }) => lighten(0.1, readableColor(theme.colors.primary))};
    cursor: not-allowed;
  }

${({ color }) => (
    (color === 'alt') && css`
    border: 2px solid ${({ theme }) => theme.colors.alt};
    background: ${({ theme }) => theme.colors.alt};

    &:disabled {
      background: ${({ theme }) => desaturate(0.5, theme.colors.alt)};
      border-color: ${({ theme }) => desaturate(0.7, theme.colors.alt)};
      color: ${({ theme }) => lighten(0.1, readableColor(theme.colors.alt))};
      cursor: not-allowed;
    }
  `)
}
  
  color: white;
  text-shadow: 0 0 1px rgba(0,0,0,0.5);

  &.huge {
    font-size: 1.8rem;
    padding: .85rem 1.35rem;
  }

  &:focus {
    outline: 0;
    box-shadow: 0 0 0 0.2rem ${({ theme }) => transparentize(0.5, theme.colors.primary)}; 
  }

  transition: 70ms transform linear;
  &:active {
    transform: translateY(3px);
  }
  `;

function Button(props) {
  return (
    <StyledButton {...props} />
  );
}

export default Button;
