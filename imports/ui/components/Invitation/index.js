import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import qrcode from 'qrcode';
import { Meteor } from 'meteor/meteor';

import Input from '../Input';
import { MAX_PLAYERS } from '../../../api/game/constants';

const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 50;

  padding: 2em;

  background: rgba(250, 251, 209, 0.85);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Header = styled.h1`
  text-align: center;
  color: ${({ theme }) => theme.colors.primary};
`;

const HelpBlock = styled.div`
  display: inline-flex;
  max-width: 80%;
`;
const QRCode = styled.div`
  padding: 78px 19px 74px 21px;

  width: 250px;
  height: 524px;
  /* http://www.pngmart.com/image/15982 */
  background-image: url(/img/phone.png);
  background-size: 100%;
  background-position: center;
  background-repeat: no-repeat;

  & > h4 {
    color: ${({ theme }) => theme.colors.primary};
    margin: 0;
  }

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const HelpContent = styled.div`
  margin: 1em;
  padding: 1.2em;
  background: rgba(254, 255, 230, 0.76);
  border-radius: 5px;
  color: #460f0f;
  text-align: justify;
`;

class Invitation extends React.Component {
  static propTypes = {
    room: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      slug: PropTypes.string.isRequired,
      players: PropTypes.array.isRequired,
    }).isRequired,
  }

  componentDidMount() {
    // eslint-disable-next-line no-underscore-dangle
    const url = Meteor.absoluteUrl(`_/${this.props.room.slug}`);
    console.info('Controller URL:', url);
    qrcode.toCanvas(this.qrCanvas, url, {
      scale: 5,
      color: {
        dark: '#02190d',
        light: '#fffff7',
      },
    }, (error) => {
      if (error) console.error(error);
    });
  }

  onRoomUrlClick = (e) => {
    e.currentTarget.setSelectionRange(0, e.currentTarget.value.length);
  }

  render() {
    const { room } = this.props;
    return (
      <Container>
        <Header>Ожидание игроков...</Header>
        <HelpBlock>
          <QRCode>
            <h4>
              Просканируй меня!
            </h4>
            <canvas ref={(ref) => { this.qrCanvas = ref; }} />
          </QRCode>
          <HelpContent>
            <p>
              Для начала игры необходимо зайти в комнату со своего смартфона,{' '}
              который будет использоваться в качестве джойстика.
            </p>
            <p>
              Просканируйте <strong>QR-код</strong> слева и{' '}
              <strike>заставьте</strike> предложите другу сделать то же самое.
            </p>
            <h3>Игроков в комнате: {room.players.length}/{MAX_PLAYERS}</h3>
            <p>
              Если вы далеко друг от друга - тоже не беда. Отправьте ему эту ссылку:
            </p>
            <p>
              <Input
                onClick={this.onRoomUrlClick}
                value={Meteor.absoluteUrl(room.slug)}
                readOnly
              />
            </p>
          </HelpContent>
        </HelpBlock>
      </Container>
    );
  }
}

export default Invitation;
