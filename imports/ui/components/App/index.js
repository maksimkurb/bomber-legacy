import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import 'normalize.css';

import theme from '../../theme';
import Layout from '../Layout';

import Home from '../../pages/home/loadable';
import Game from '../../pages/game/loadable';
import Controller from '../../pages/controller/loadable';

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Layout>
          <Route exact path="/" component={Home} />
          <Route exact path="/_/:slug" component={Controller} />
          <Route exact path="/:slug" component={Game} />
        </Layout>
      </Router>
    </ThemeProvider>
  );
}
