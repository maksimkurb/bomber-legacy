import React from 'react';
import PropTypes from 'prop-types';
import styled, { injectGlobal } from 'styled-components';
import { lighten } from 'polished';

injectGlobal([`
* {
  box-sizing: border-box;
}
html {
  height: 100%;
}
body,
#render-target  {
  min-height: 100%;
  height: 100%;
}
body {
  font-size: 18px;
  font-family: 'Roboto', sans-serif;
}
`]);

const Container = styled.div`
  min-height: 100%;
  height: 100%;
  background: ${({ theme }) => theme.colors.bg};
  background: radial-gradient(${({ theme }) => lighten(0.1, theme.colors.bg)},  ${({ theme }) => theme.colors.bg});

`;

function Layout({ children }) {
  return (
    <Container>
      {children}
    </Container>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
