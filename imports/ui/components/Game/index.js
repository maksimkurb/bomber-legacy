import React from 'react';
import PropTypes from 'prop-types';
import { TimeSync } from 'meteor/mizzao:timesync';
import styled from 'styled-components';

import Game, { countMaxRect } from '../../../game';

const Container = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
`;
const CanvasContainer = styled.div`
  margin: auto;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  box-sizing: content-box;
  border: 3px double #c8b8a0;
`;
class GameComponent extends React.Component {
  static propTypes = {
    room: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      slug: PropTypes.string.isRequired,
      size: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired,
      }).isRequired,
      map: PropTypes.array,
    }).isRequired,
  }

  componentDidMount() {
    const { room } = this.props;
    this.rect = countMaxRect(
      room.size.x, room.size.y,
      this.root.clientWidth, this.root.clientHeight,
    );
    this.game = new Game(this.canvas, this.rect, room);

    this.game.onMount();
    this.timerHdl = setInterval(() => {
      console.log('Time offset is', TimeSync.serverOffset());
    }, 5000);

    this.onResize();
    window.addEventListener('resize', this.onResize);
  }

  componentWillReceiveProps(nextProps) {
    if (this.game && nextProps.room) {
      this.game.onRoomUpdate(nextProps.room);
    }
  }

  shouldComponentUpdate() {
    return false;
  }

  componentWillUnmount() {
    this.game.onUnmount();
    window.removeEventListener('resize', this.onResize);
    window.removeEventListener('keydown', this.onKeyDown);

    if (this.timerHdl) {
      clearInterval(this.timerHdl);
    }
  }

  onResize = () => {
    const origAspect = this.rect.w / this.rect.h;

    // We MUST bound to this sizes
    const w = this.root.clientWidth;
    const h = this.root.clientHeight;

    let nw = 0;
    let nh = 0;

    nw = Math.min((h / this.rect.h) * this.rect.w, w);
    nh = nw / origAspect;

    this.canvas.style.width = `${nw}px`;
    this.canvas.style.height = `${nh}px`;
    this.game.onResize(nw, nh, Math.min(nw / this.rect.w, nh / this.rect.h));
  };

  origAspectRatio = 0;

  render() {
    return (
      <Container
        innerRef={(c) => {
          this.root = c;
        }}
      >
        <CanvasContainer
          innerRef={(c) => {
            this.canvas = c;
          }}
        />
      </Container>
    );
  }
}

export default GameComponent;
