import randomstring from 'randomstring';

export function persistSession() {
  if (!localStorage.getItem('session')) {
    localStorage.setItem('session', randomstring.generate(16));
  }
}

export function getSession() {
  return localStorage.getItem('session');
}
