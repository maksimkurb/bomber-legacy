// Deprecated due to textures :)

export default {
  solid: '#32422f',
  fragile: '#239D60',

  player: [
    '#f11976',
    '#1990f1',
    '#fdc530',
    '#15e449',
  ],
};
