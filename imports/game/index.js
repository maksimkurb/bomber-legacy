import Two from 'two.js';
import { TimeSync } from 'meteor/mizzao:timesync';

import textures from './textures';

import {
  BLOCK_NONE,
  BLOCK_SOLID,
  BLOCK_FRAGILE,
  SCALE_BLOCK,
  SCALE_PLAYER,
  SCALE_BOMB,
} from './constants';

import animateBomb from './animateBomb';

export const DIR_LEFT = 0;
export const DIR_UP = 1;
export const DIR_RIGHT = 2;
export const DIR_DOWN = 3;


export function countMaxRect(x, y, width, height, blockSize = 64) {
  const w = blockSize * x;
  const h = blockSize * y;

  return {
    x, y, w, h, blockSize,
  };
}

export default class Game {
  mapObjects = [];
  playerObjects = [];
  bombObjects = {};
  bombAnimationHandles = {};
  room = null;
  initialized = false;

  constructor(container, rect, room) {
    this.container = container;
    if (!rect) {
      throw new Error('You must include rect parameters');
    }
    this.rect = rect;

    if (!room) {
      throw new Error('You must provide room state');
    }
    this.room = room;

    this.two = new Two({
      width: rect.w,
      height: rect.h,
      type: Two.Types.canvas,
    }).appendTo(this.container);
    // We must add bombsGroup earlier to make bombs having smaller z-index
    this.bombsGroup = this.two.makeGroup();
    this.objectsGroup = this.two.makeGroup();
    this.generateMapObjects();
    this.generatePlayers();

    this.initialized = true;
  }

  onResize(w, h, scale) {
    this.bombsGroup.scale = scale;
    this.objectsGroup.scale = scale;
    this.two.width = w;
    this.two.height = h;
    this.two.renderer.setSize(w, h);
    this.update();
  }

  onDetonate(id) {
    const { bomb } = this.players[id];

    for (let i = 0; i < 10; i++) {
      let x;
      let y;
      if (i < 5) {
        x = bomb.pos.x + (i - 2);
        y = bomb.pos.y; // eslint-disable-line prefer-destructuring
      } else {
        if (i === 7) continue;
        x = bomb.pos.x; // eslint-disable-line prefer-destructuring
        y = bomb.pos.y + (i - 7);
      }
      if (
        // Overflow check
        x >= 0 &&
        y >= 0 &&
        x < this.rect.x &&
        y < this.rect.y &&
        // and if fragile
        this.room.map[x][y] === BLOCK_FRAGILE
      ) {
        this.objectsMap[x][y].remove();
        this.objectsMap[x][y] = null;
        this.room.map[x][y] = BLOCK_NONE;
      }
    }
    bomb.obj.remove();
    this.players[id].bomb = null;
    this.update();
  }

  size(x = 0, y = 0, w = 1, h = null) {
    if (h === null) {
      h = w; // eslint-disable-line no-param-reassign
    }
    const { blockSize } = this.rect;
    return {
      x: (x * blockSize) + (blockSize / 2),
      y: (y * blockSize) + (blockSize / 2),
      w: (w * blockSize),
      h: (h * blockSize),
    };
  }

  generateMapObjects() {
    const mapObjects = [];


    for (let i = 0; i < this.rect.x; i++) {
      const mapRow = this.room.map[i];
      const row = [];
      for (let j = 0; j < this.rect.y; j++) {
        const {
          x, y, w, h,
        } = this.size(i, j, SCALE_BLOCK);
        if (mapRow[j] === BLOCK_SOLID) {
          const solid = this.two.makeRectangle(x, y, w, h);
          solid.fill = textures.solid;
          solid.stroke = 'none';
          solid.blockType = BLOCK_SOLID;
          this.objectsGroup.add(solid);
          row.push(solid);
        } else if (mapRow[j] === BLOCK_FRAGILE) {
          const fragile = this.two.makeRectangle(x, y, w, h);
          fragile.fill = textures.fragile;
          fragile.stroke = 'none';
          fragile.blockType = BLOCK_FRAGILE;
          this.objectsGroup.add(fragile);
          row.push(fragile);
        } else {
          row.push(null);
        }
      }
      mapObjects.push(row);
    }
    this.mapObjects = mapObjects;
  }

  generatePlayer(id, player) {
    if (player.dead) {
      this.playerObjects[id] = { dead: true };
      return;
    }
    const {
      x: nx, y: ny, w, h,
    } = this.size(player.pos.x, player.pos.y, SCALE_PLAYER);
    const plObj = this.two.makeRectangle(nx, ny, w, h);
    // player.fill = theme.player[id];
    plObj.fill = textures.player[id].down;
    plObj.stroke = 'none';
    this.objectsGroup.add(plObj);
    this.playerObjects[id] = plObj;
  }

  generatePlayers() {
    for (let i = 0; i < this.room.players.length; i++) {
      if (this.playerObjects[i]) continue;
      this.generatePlayer(i, this.room.players[i]);
    }
  }

  update() {
    this.two.update();
  }

  onMount() {
    this.update();
  }

  onRoomUpdate(newRoom) {
    if (!this.initialized) return;
    this.room = newRoom;

    // Remove old bombs object
    const bombIds = this.room.players
      .reduce((prev, pl) => prev.concat(pl.bombs), [])
      .map(b => b.bombId);

    Object.keys(this.bombObjects).forEach((bombId) => {
      if (bombIds.indexOf(bombId) === -1) {
        this.bombObjects[bombId].remove();
        if (this.bombAnimationHandles[bombId]) {
          clearTimeout(this.bombAnimationHandles[bombId]);
          delete this.bombAnimationHandles[bombId];
        }
      }
    });

    // Update players
    if (this.room.players.length !== this.playerObjects.length) {
      this.generatePlayers();
    }

    for (let i = 0; i < this.room.players.length; i++) {
      const player = this.room.players[i];
      const pObj = this.playerObjects[i];

      if (!pObj.dead) {
        const {
          x: nx, y: ny,
        } = this.size(player.pos.x, player.pos.y, SCALE_PLAYER);
        pObj.translation.set(nx, ny);
        pObj.fill = textures.player[i][player.dir];
        if (player.dead) {
          pObj.remove();
          pObj.dead = true;
        }
      }

      // Draw player bombs
      for (let k = 0; k < player.bombs.length; k++) {
        const bomb = player.bombs[k];

        if (!this.bombObjects[bomb.bombId]) {
          {
            const {
              x: nx, y: ny, w, h,
            } = this.size(bomb.pos.x, bomb.pos.y, SCALE_BOMB);
            const bombObj = this.two.makeRectangle(nx, ny, w, h);
            bombObj.fill = textures.bomb;
            bombObj.stroke = 'none';
            this.bombsGroup.add(bombObj);
            this.bombObjects[bomb.bombId] = bombObj;
          }
          this.bombAnimationHandles[bomb.bombId] = setTimeout(() => {
            const animations = animateBomb(bomb.pos, this.room);
            const animTiles = animations.map((anim) => {
              const {
                x: nx, y: ny, w, h,
              } = this.size(anim.pos.x, anim.pos.y, SCALE_PLAYER);
              const t = this.two.makeRectangle(nx, ny, w, h);
              t.fill = textures.flame[anim.id];
              t.stroke = 'none';
              t.rotation = anim.rotation;
              this.objectsGroup.add(t);
              return t;
            });
            this.update();
            /* eslint-disable no-param-reassign */
            setTimeout(() => {
              animTiles.forEach((t) => {
                t.opacity = 0;
              });
              this.update();
            }, 300);
            setTimeout(() => {
              animTiles.forEach((t) => {
                t.opacity = 1;
              });
              this.update();
            }, 600);
            setTimeout(() => {
              animTiles.forEach((t) => {
                t.remove();
              });
              this.update();
            }, 900);
            /* eslint-enable no-param-reassign */
          }, (bomb.deployAt) - TimeSync.serverOffset() - Date.now());
        }
      }
    }

    // Update map
    for (let mx = 0; mx < this.room.size.x; mx++) {
      for (let my = 0; my < this.room.size.y; my++) {
        if (this.mapObjects[mx][my] && !this.room.map[mx][my]) {
          this.mapObjects[mx][my].remove();
          this.mapObjects[mx][my] = null;
        } else if (!this.mapObjects[mx][my] && this.room.map[mx][my]) {
          const {
            x, y, w, h,
          } = this.size(mx, my, SCALE_BLOCK);
          if (this.room.map[mx][my] === BLOCK_SOLID) {
            const solid = this.two.makeRectangle(x, y, w, h);
            solid.fill = textures.solid;
            solid.stroke = 'none';
            solid.blockType = BLOCK_SOLID;
            this.objectsGroup.add(solid);
            this.mapObjects[mx][my] = solid;
          } else if (this.room.map[mx][my] === BLOCK_FRAGILE) {
            const fragile = this.two.makeRectangle(x, y, w, h);
            fragile.fill = textures.fragile;
            fragile.stroke = 'none';
            fragile.blockType = BLOCK_FRAGILE;
            this.objectsGroup.add(fragile);
            this.mapObjects[mx][my] = fragile;
          }
        }
      }
    }

    this.update();
  }

  onUnmount = () => {
    // stub
  }
}
