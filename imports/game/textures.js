import Two from 'two.js';

import {
  SCALE_BLOCK,
  SCALE_PLAYER,
  SCALE_BOMB,
} from './constants';


const textures = {
  solid: new Two.Texture('/textures/solid.png'),
  fragile: new Two.Texture('/textures/fragile.png'),

  bomb: new Two.Texture('/textures/bomb.png'),

  flame: [
    new Two.Texture('/textures/flame0.png'),
    new Two.Texture('/textures/flame1.png'),
    new Two.Texture('/textures/flame2.png'),
  ],

  player: [0, 1, 2, 3].map(i => ({
    up: new Two.Texture(`/textures/player${i}-up.png`),
    right: new Two.Texture(`/textures/player${i}-right.png`),
    down: new Two.Texture(`/textures/player${i}-down.png`),
    left: new Two.Texture(`/textures/player${i}-left.png`),
  })),
};

textures.solid.scale = SCALE_BLOCK;
textures.fragile.scale = SCALE_BLOCK;
textures.bomb.scale = SCALE_BOMB;
textures.player.forEach((pl) => {
  /* eslint-disable no-param-reassign */
  pl.up.scale = SCALE_PLAYER;
  pl.right.scale = SCALE_PLAYER;
  pl.down.scale = SCALE_PLAYER;
  pl.left.scale = SCALE_PLAYER;
  /* eslint-enable no-param-reassign */
});

const playerImages = [0, 1, 2, 3].map(i => `/textures/player${i}-down.png`);
export { playerImages };
export default textures;

