export const DEFAULT_SIZE_X = 19;
export const DEFAULT_SIZE_Y = 15;

export const BLOCK_NONE = 0;
export const BLOCK_SOLID = 1;
export const BLOCK_FRAGILE = 2;

export const SCALE_FLAME = 1;
export const SCALE_PLAYER = 1.1;
export const SCALE_BLOCK = 0.95;
export const SCALE_BOMB = 0.6;
