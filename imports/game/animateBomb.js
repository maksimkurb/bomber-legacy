import { BLOCK_SOLID } from './constants';

/* eslint-disable prefer-destructuring, no-param-reassign */

function deployAtDirection(pos, room, px, py) {
  const objects = [];
  for (let i = ((px === 1 && py === 0) ? 0 : 1); i <= 2; i++) {
    const x = pos.x + (i * px);
    const y = pos.y + (i * py);
    if (
      // Overflow check
      x < 0 ||
      y < 0 ||
      x >= room.size.x ||
      y >= room.size.y ||
      // of if solid
      room.map[x][y] === BLOCK_SOLID
    ) break;
    let rotation = 0;
    if (py === 1) {
      rotation = -Math.PI / 2;
    } else if (py === -1) {
      rotation = Math.PI / 2;
    } else if (px === 1) {
      rotation = -Math.PI;
    } else if (px === -1) {
      rotation = 0;
    }
    const animPos = {
      pos: {
        x, y,
      },
      id: i,
      rotation,
    };
    objects.push(animPos);
  }
  return objects;
}
function animateBomb(pos, room) {
  let objects = [];
  objects = objects.concat(deployAtDirection(pos, room, 1, 0));
  objects = objects.concat(deployAtDirection(pos, room, -1, 0));
  objects = objects.concat(deployAtDirection(pos, room, 0, 1));
  objects = objects.concat(deployAtDirection(pos, room, 0, -1));
  return objects;
}

export default animateBomb;
