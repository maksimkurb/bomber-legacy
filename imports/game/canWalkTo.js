import { BLOCK_NONE } from './constants';

function canWalkTo(room, x, y, isForBomb = false) {
  if (x < 0 || y < 0) return false;
  if (x >= room.size.x || y >= room.size.y) return false;

  // Check that there is no blocks at position
  if (room.map[x][y] !== BLOCK_NONE) return false;

  // Check that there is no bombs at position
  const bombs = room.players.reduce((prev, player) => prev.concat(player.bombs), []);
  if (bombs.find(bomb => bomb.pos.x === x && bomb.pos.y === y)) return false;

  // Check that there is no players at position
  if (!isForBomb && room.players.find(pl => pl.pos.x === x && pl.pos.y === y)) return false;

  return true;
}

export default canWalkTo;
