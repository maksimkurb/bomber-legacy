// Definition of the links collection

import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

// If you want change name, also change it in publications.js
const Room = new Mongo.Collection('rooms', Meteor.isServer ? { connection: null } : undefined);
export default Room;
