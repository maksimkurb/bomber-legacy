// Methods related to links

import { Meteor } from 'meteor/meteor';
import { Match, check } from 'meteor/check';
import randomstring from 'randomstring';

import Room from './room';

import takeRandomWords from './server/words';
import { DEFAULT_SIZE_X, DEFAULT_SIZE_Y, BLOCK_FRAGILE, BLOCK_NONE, BLOCK_SOLID } from '../../game/constants';
import generateMap from '../../game/generateMap';
import canWalkTo from '../../game/canWalkTo';
import { MAX_PLAYERS, MAX_CONCURRENT_BOMBS, DEPLOY_TIME_SECS } from './constants';
import deployBomb from '../../game/deployBomb';

const NonEmptyString = Match.Where((x) => {
  check(x, String);
  return x.length > 0;
});

const validActions = [
  'up',
  'right',
  'down',
  'left',
  'plant',
];
const ActionString = Match.Where((x) => {
  check(x, String);
  return validActions.includes(x);
});

Meteor.methods({
  'room.generateSlug': () => takeRandomWords(3).join('-'),

  'room.createBySlug': async (slug) => {
    check(slug, NonEmptyString);

    const room = Room.findOne({ slug });
    if (!room) {
      Room.insert({
        slug,
        size: {
          x: DEFAULT_SIZE_X,
          y: DEFAULT_SIZE_Y,
        },
        active: false,
        players: [],
        map: generateMap(DEFAULT_SIZE_X, DEFAULT_SIZE_Y),
      });
    }
  },

  'room.restart': async (_id) => {
    check(_id, NonEmptyString);

    const room = Room.findOne({ _id });
    if (room) {
      Room.update(
        { _id },
        {
          size: {
            x: DEFAULT_SIZE_X,
            y: DEFAULT_SIZE_Y,
          },
          active: false,
          players: [],
          map: generateMap(DEFAULT_SIZE_X, DEFAULT_SIZE_Y),
        },
      );
    }
  },

  'room.join': (_id, session) => {
    check(_id, NonEmptyString);
    check(session, NonEmptyString);

    const room = Room.findOne({ _id });
    if (!room) return new Error('Room is not found');

    // Rejoin user if it is left before
    const userIdx = room.players.findIndex(player => player.session === session);
    if (userIdx !== -1) {
      Room.update({ _id, 'players.session': session }, {
        $set: {
          'players.$.active': true,
        },
      });
      return true;
    }

    if (room.active) return new Error('Could not connect to active room');
    if (room.players.length === MAX_PLAYERS) return new Error('Room is full');

    let pos;
    switch (room.players.length) {
      case 0:
        pos = { x: 0, y: 0 };
        break;
      case 1:
        pos = { x: room.size.x - 1, y: room.size.y - 1 };
        break;
      case 2:
        pos = { x: room.size.x - 1, y: 0 };
        break;
      case 3:
        pos = { x: 0, y: room.size.y - 1 };
        break;
      default:
        throw new Error(`Player #${room.players.length} position is not hardcoded; sorry ;(`);
    }

    Room.update({ _id }, {
      $push: {
        players: {
          session,
          pos,
          dir: 'down',
          dead: false,
          ready: false,
          bombs: [],
        },
      },
    });

    return true;
  },
  'room.leave': (_id, session) => {
    check(_id, NonEmptyString);
    check(session, NonEmptyString);

    const room = Room.findOne({ _id });
    if (!room) return new Error('Room is not found');

    // Rejoin user if it is left before
    const userIdx = room.players.findIndex(player => player.session === session);
    if (userIdx === -1) {
      return new Error('Player is not joined that room');
    }
    Room.update({ _id, 'players.session': session }, {
      $set: {
        'players.$.active': false,
      },
    });
    return true;
  },
  'room.toggleReady': (_id, session) => {
    check(_id, NonEmptyString);
    check(session, NonEmptyString);

    const room = Room.findOne({ _id });
    if (!room) return new Error('Room is not found');

    // Rejoin user if it is left before
    const userIdx = room.players.findIndex(player => player.session === session);
    if (userIdx === -1) {
      return new Error('Player is not joined that room');
    }

    // Inverse
    room.players[userIdx].ready = !room.players[userIdx].ready;
    const allIsReady = room.players.reduce(((ready, p) => ready && (p.ready)), true);
    Room.update({ _id, 'players.session': session }, {
      $set: {
        'players.$.ready': room.players[userIdx].ready,
        active: allIsReady,
      },
    });
    return true;
  },

  'room.handleAction': (_id, session, action) => {
    check(_id, NonEmptyString);
    check(session, NonEmptyString);
    check(action, ActionString);

    const room = Room.findOne({ _id });
    if (!room) return new Error('Room is not found');

    const userIdx = room.players.findIndex(player => player.session === session);
    if (userIdx === -1) {
      return new Error('Player is not joined that room');
    }

    const { pos } = room.players[userIdx];
    let { dir } = room.players[userIdx];

    // Calculations
    switch (action) {
      case 'up':
        if (canWalkTo(room, pos.x, pos.y - 1)) { pos.y -= 1; }
        dir = 'up';
        break;
      case 'right':
        if (canWalkTo(room, pos.x + 1, pos.y)) { pos.x += 1; }
        dir = 'right';
        break;
      case 'down':
        if (canWalkTo(room, pos.x, pos.y + 1)) { pos.y += 1; }
        dir = 'down';
        break;
      case 'left':
        if (canWalkTo(room, pos.x - 1, pos.y)) { pos.x -= 1; }
        dir = 'left';
        break;
      default:
    }

    // Update
    switch (action) {
      case 'up':
      case 'right':
      case 'down':
      case 'left':
        Room.update({ _id, 'players.session': session }, {
          $set: {
            'players.$.pos': pos, // new player pos
            'players.$.dir': dir,
          },
        });
        break;
      case 'plant':
        // Check that bomb is not already placed here
        if (!canWalkTo(room, pos.x, pos.y, true)) return false;
        if (room.players[userIdx].bombs.length < MAX_CONCURRENT_BOMBS) {
          const bombId = randomstring.generate(8);
          Room.update({ _id, 'players.session': session }, {
            $push: {
              'players.$.bombs': {
                bombId,
                // placedAt: Date.now(),
                deployAt: Date.now() + (1000 * DEPLOY_TIME_SECS),
                pos, // player pos
              },
            },
          });

          setTimeout(() => {
            const currentRoom = Room.findOne({ _id });
            const deadPlayers = deployBomb(pos, currentRoom);
            Room.update({ _id, 'players.session': session }, {
              $pull: {
                'players.$.bombs': {
                  bombId,
                },
              },
              $set: {
                map: currentRoom.map,
              },
            });
            deadPlayers.forEach((plIdx) => {
              Room.update({ _id }, {
                $set: {
                  [`players.${plIdx}.dead`]: true,
                },
              });
            });
          }, 1000 * (DEPLOY_TIME_SECS + 0.05));
        }
        break;
      default:
    }
    return true;
  },
});
