/* eslint-disable import/prefer-default-export */

export const MAX_PLAYERS = 4;
export const MAX_CONCURRENT_BOMBS = 2;
export const DEPLOY_TIME_SECS = 3;
