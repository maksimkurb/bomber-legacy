// Tests for the behavior of the links collection
//
// https://guide.meteor.com/testing.html

import { Meteor } from 'meteor/meteor';
import { assert } from 'chai';
import { Room } from './room';

if (Meteor.isServer) {
  describe('room collection', () => {
    it('insert correctly', () => {
      const roomId = Room.insert({
        title: 'meteor homepage',
        url: 'https://www.meteor.com',
      });
      const added = Room.find({ _id: roomId });
      const collectionName = added._getCollectionName();
      const count = added.count();

      assert.equal(collectionName, 'room');
      assert.equal(count, 1);
    });
  });
}
