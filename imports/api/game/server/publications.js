// All links-related publications

import { Meteor } from 'meteor/meteor';
import { Match, check } from 'meteor/check';

import Room from '../room';

const NonEmptyString = Match.Where((x) => {
  check(x, String);
  return x.length > 0;
});

Meteor.publish('room', (slug) => {
  check(slug, NonEmptyString);
  return Room.find({ slug });
});

const controllerFields = {
  _id: 1,
  slug: 1,
  players: 1,
  active: 1,
};
const controllerTransform = session => room => ({
  ...room,
  players: room.players.map(player => ({
    ready: player.ready,
    dead: player.dead,
    me: player.session === session,
    session: undefined,
  })),
});
Meteor.publish('room.controller', function roomBySlugForController(slug, session) {
  check(slug, NonEmptyString);
  check(session, NonEmptyString);

  const transform = controllerTransform(session);

  const observer = Room.find({ slug }, { fields: controllerFields }).observe({
    added: (doc) => {
      this.added('rooms', doc._id, transform(doc));
    },
    changed: (newDoc, oldDoc) => {
      this.changed('rooms', oldDoc._id, transform(newDoc));
    },
    removed: (oldDoc) => {
      this.removed('rooms', oldDoc._id);
    },
  });

  this.onStop(() => {
    observer.stop();
  });

  this.ready();
});
