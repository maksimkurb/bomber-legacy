// Tests for the links publications
//
// https://guide.meteor.com/testing.html

import { assert } from 'chai';
import { room } from '../room';
import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import './publications';

describe('room publications', () => {
  beforeEach(() => {
    room.remove({});
    room.insert({
      title: 'meteor homepage',
      url: 'https://www.meteor.com',
    });
  });

  describe('room.all', () => {
    it('sends all rooms', (done) => {
      const collector = new PublicationCollector();
      collector.collect('room.all', (collections) => {
        assert.equal(collections.links.length, 1);
        done();
      });
    });
  });
});
